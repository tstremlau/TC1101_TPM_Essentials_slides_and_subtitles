1
00:00:00,399 --> 00:00:05,839
Welcome to this OST2 course about the
TPMs built-in protection against Machine-

2
00:00:05,839 --> 00:00:09,719
in-the-Middle attacks.
There is more than one way the

3
00:00:09,719 --> 00:00:14,799
TPM can protect our communication from
such attacks. However one of these

4
00:00:14,799 --> 00:00:19,039
approaches provides the highest
guarantees. This feature is called

5
00:00:19,039 --> 00:00:24,359
parameter encryption. Before we can
answer what parameter encryption is we

6
00:00:24,359 --> 00:00:30,920
need to understand what TPM sessions are.
There are three types of TPM sessions,

7
00:00:30,920 --> 00:00:37,320
one of them is inherent, because the
authorization slots in any TPM command

8
00:00:37,320 --> 00:00:41,879
is a maximum of three. We can supply
authorization for the parent, we can

9
00:00:41,879 --> 00:00:47,199
supply authorization for the child, and
maybe we supply a policy. When we supply

10
00:00:47,199 --> 00:00:52,280
a password, we don't need to actually
start a TPM session, we just provide the

11
00:00:52,280 --> 00:00:58,480
password in plain form. As you can
imagine this is not secure. Therefore we

12
00:00:58,480 --> 00:01:02,840
can use HMAC session
to protect the communication.

13
00:01:02,840 --> 00:01:08,159
HMAC session takes authorization value from
an object to feed its key derivation

14
00:01:08,159 --> 00:01:13,439
function to create a session key. Then we
have a key exchange between the host and

15
00:01:13,439 --> 00:01:19,520
the TPM. This is a one-time transaction
once this happens the HMAC session

16
00:01:19,520 --> 00:01:24,600
refreshes with nonce from the caller and
from the receiver. Meaning from the host

17
00:01:24,600 --> 00:01:29,240
and from the TPM. This is to prevent
replay attacks. On top of an HMAC session,

18
00:01:29,240 --> 00:01:34,040
we can have a policy. And this is then
called a policy session. The policy

19
00:01:34,040 --> 00:01:40,079
session feeds itself with more than auth
value for entropy; it can take the state

20
00:01:40,079 --> 00:01:45,240
of the TPM, it can take various other
parameters, and we'll talk more about

21
00:01:45,240 --> 00:01:49,479
this in our advanced course. The policy
session can be used for authorization,

22
00:01:49,479 --> 00:01:53,159
but before we can perform any
authorization we would need to craft a

23
00:01:53,159 --> 00:01:57,600
policy digest. And for that purpose we
have a trial session, which is used to

24
00:01:57,600 --> 00:02:02,520
generate the policy digest. In a way we
can say a trial is a policy session

25
00:02:02,520 --> 00:02:08,160
only for computation. To put things into
perspective when we issue a TPM command

26
00:02:08,160 --> 00:02:12,599
there are three authorization slots that
we can use. Most commands use only one

27
00:02:12,599 --> 00:02:18,200
authorization slots, some use two, and
rarely we need all three. To protect the

28
00:02:18,200 --> 00:02:22,400
communication of the TPM we can put a
HMAC session in one of these

29
00:02:22,400 --> 00:02:27,920
authorization slots. To start HMAC
session we would need to learn about the

30
00:02:27,920 --> 00:02:33,120
tpm2_startauthsession command
and we will do that in a moment. Because

31
00:02:33,120 --> 00:02:37,800
a policy session is built on top of an
HMAC session, we can also use a policy

32
00:02:37,800 --> 00:02:42,080
session to protect against machine-in-
the-middle attacks. The highest guarantee

33
00:02:42,080 --> 00:02:46,599
we can receive is when we enable
parameter encryption. So what is

34
00:02:46,599 --> 00:02:50,879
parameter encryption? There's two flags
that we can set to a session that

35
00:02:50,879 --> 00:02:57,200
instructs the TPM to expect encrypted
parameters to incoming commands.

36
00:02:57,200 --> 00:03:02,879
Or, instruct the TPM to send results from
the commands, the responses, with

37
00:03:02,879 --> 00:03:07,799
encrypted parameters. There is a
limitation. We can only encrypt the very

38
00:03:07,799 --> 00:03:12,440
first parameter of any command that
supports parameter encryption. Why not

39
00:03:12,440 --> 00:03:16,720
all commands support parameter
encryption? Because not every command has

40
00:03:16,720 --> 00:03:21,959
sensitive information. Let's think about
TPM nvincrement that increases the

41
00:03:21,959 --> 00:03:27,920
counter in the TPMs NVRAM by one. Other
than the authorization values to

42
00:03:27,920 --> 00:03:32,040
authorize this action, there's nothing
sensitive in this command so we can

43
00:03:32,040 --> 00:03:36,599
protect this command by simply creating
an HMAC session. And we'll see how to do

44
00:03:36,599 --> 00:03:41,480
that later on. In other cases when we
create a key we need to provide not just

45
00:03:41,480 --> 00:03:44,560
the authorization for the parent but
also we need to provide new

46
00:03:44,560 --> 00:03:49,400
authorization for the child key. This
resides at the beginning of the command,

47
00:03:49,400 --> 00:03:54,480
in the first parameter in a field in a
place called inSensitive. To protect that

48
00:03:54,480 --> 00:03:59,079
field from Machine-in-the-Middle attacks,
we can encrypt. To achieve this we need

49
00:03:59,079 --> 00:04:04,480
to supply the TPM command with a TPM
session. This TPM session needs to have

50
00:04:04,480 --> 00:04:08,799
the two flags for encrypting the
parameter request, and the commands'

51
00:04:08,799 --> 00:04:13,480
response, set. We're going to take a
closer look at the TPM commands to see

52
00:04:13,480 --> 00:04:17,959
what and how parameter encryption can
protect. But before that I want to

53
00:04:17,959 --> 00:04:23,000
address the topic of TPM sniffing.
Contrary to popular beliefs, TPM sniffing

54
00:04:23,000 --> 00:04:28,759
is not possible because of failure in
the protection of the TPM or in its

55
00:04:28,759 --> 00:04:35,520
mechanisms and algorithms. Often, if not
always, it is the result of misconfiguration.

56
00:04:35,520 --> 00:04:41,160
OEMs and OS vendors fail to enable
parameter encryption, fail to enable

57
00:04:41,160 --> 00:04:46,520
early, or forget to enable at all.
It makes sense to have the UEFI or BIOS set

58
00:04:46,520 --> 00:04:50,400
up parameter encryption because it is a
one-time operation. Once we have a

59
00:04:50,400 --> 00:04:54,560
session with parameter encryption
enabled we can reuse that session over

60
00:04:54,560 --> 00:04:58,919
and over for different commands. Because
the nonces are being replaced. We have

61
00:04:58,919 --> 00:05:03,280
fresh nonce for every new command, both
from the TPM and from the caller, from

62
00:05:03,280 --> 00:05:09,440
the host. So this misconfiguration or
lack of feature enabling happens at

63
00:05:09,440 --> 00:05:14,639
multiple stages. First it happens at the
OEM level, then it can happen at the

64
00:05:14,639 --> 00:05:19,400
bootloader level, or even at the OS level.
To address this issue there is a new

65
00:05:19,400 --> 00:05:23,919
feature coming to the TPM that will
provide a complete bus encryption

66
00:05:23,919 --> 00:05:30,319
without the need for OEM and OS vendors
to take extra steps. At the same time,

67
00:05:30,319 --> 00:05:35,600
even today, we can protect ourselves
against TPM sniffing by simply using the

68
00:05:35,600 --> 00:05:40,600
TPM properly. The bottom line is that
when parameter encryption is enabled TPM

69
00:05:40,600 --> 00:05:45,039
sniffing is not possible. Let's now take
a closer look how parameter encryption

70
00:05:45,039 --> 00:05:49,960
protects us from this danger. Here we
have the command request that is sent

71
00:05:49,960 --> 00:05:55,039
from the host, usually our secure
application, to the TPM. As you can see,

72
00:05:55,039 --> 00:05:59,080
there are three standard fields in each
command this is the tag, the command size,

73
00:05:59,080 --> 00:06:04,120
and the command code. Afterwards comes the
authorization area. This authorization

74
00:06:04,120 --> 00:06:09,639
area, as mentioned earlier, can have up to
three authorizations. In this case we

75
00:06:09,639 --> 00:06:15,160
have one authorization being used. And
this is the authorization for the parent

76
00:06:15,160 --> 00:06:19,919
key. Remember we need a parent key to
generate a child key. So where does the

77
00:06:19,919 --> 00:06:24,800
authorization for the new child key goes?
In the first parameter of this command

78
00:06:24,800 --> 00:06:30,960
called inSensitive. This is actually "in"
as input and "Sensitive" as critical data

79
00:06:30,960 --> 00:06:35,400
that we need to keep
private. When we send this command to the

80
00:06:35,400 --> 00:06:42,039
TPM and it is not encrypted, the password
authorization for the child key is there

81
00:06:42,039 --> 00:06:48,080
in plain form. What parameter encryption
does is perform encryption using the

82
00:06:48,080 --> 00:06:52,919
session key that is created during the
creation of the TPM session, for

83
00:06:52,919 --> 00:06:57,720
parameter encryption. The TPM expects the
first parameter of this command to be

84
00:06:57,720 --> 00:07:02,800
encrypted and uses the exchanged session
key, between the host and the TPM at the

85
00:07:02,800 --> 00:07:06,960
beginning of that session when it was
created, to decrypt the parameters. After

86
00:07:06,960 --> 00:07:11,720
execution of the command, the TPM has the
possibility to also respond with first

87
00:07:11,720 --> 00:07:18,240
encrypted parameter back. And we see here
why. The first parameter of the response

88
00:07:18,240 --> 00:07:23,360
of the TPM2_Create command is the
actual private material of the new child

89
00:07:23,360 --> 00:07:30,080
key. Let's take one more example. Here is
the TPM2_NV_Write command, that enables us to

90
00:07:30,080 --> 00:07:36,240
write bytes in bulk to the NVRAM of
the TPM. Here we have two authorization

91
00:07:36,240 --> 00:07:41,639
slots being used. And we can use the
third one to add a session for parameter

92
00:07:41,639 --> 00:07:47,879
encryption. The first parameter of this
command is the data, the input data, our

93
00:07:47,879 --> 00:07:54,639
own very sensitive user data. By having
TPM parameter encryption enabled, we can

94
00:07:54,639 --> 00:07:59,639
protect that data and send it in
encrypted form to the TPM. The TPM will

95
00:07:59,639 --> 00:08:05,280
internally decrypt and store to its
internal memory. This way the data is

96
00:08:05,280 --> 00:08:11,120
never exposed to Machine-in-the-Middle
attacks. Here is an example of a command (TPM2_NV_Increment)

97
00:08:11,120 --> 00:08:18,400
that cannot use parameter encryption. It
can, however, use a TPM HMAC session, to

98
00:08:18,400 --> 00:08:23,639
protect the overall communication. And in
particular to protect the authorizations

99
00:08:23,639 --> 00:08:29,360
for this command. We don't have any data
fields any parameter fields here. We just

100
00:08:29,360 --> 00:08:34,640
have the header of the command, tag,
commandSize, commandCode, and two

101
00:08:34,640 --> 00:08:38,560
authorization slots.
Therefore it is recommended to

102
00:08:38,560 --> 00:08:43,440
always have an HMAC session started
with parameter encryption enabled. So how

103
00:08:43,440 --> 00:08:48,880
to use this powerful feature the TPM. As
mentioned we need to start a TPM session.

104
00:08:48,880 --> 00:08:52,440
And there is a straightforward way to do
that, there is a dedicated command for

105
00:08:52,440 --> 00:08:57,519
this, and we have a dedicated TPM tool
we're going to look in a moment. Because

106
00:08:57,519 --> 00:09:03,000
creating an HMAC session requires the
auth value of an object to feed the

107
00:09:03,000 --> 00:09:07,440
entropy for the KDF, to generate the
session key, and exchange with the TPM, 

108
00:09:07,440 --> 00:09:11,800
and a policy session builds on top of an
HMAC session. In all of these cases, we

109
00:09:11,800 --> 00:09:15,959
would need to have a good source of
entropy. And what better source of

110
00:09:15,959 --> 00:09:20,519
entropy than a primary key that is
created inside the TPM, and its private

111
00:09:20,519 --> 00:09:25,440
material never leaves the TPM. So the
seed, the entropy, for our parameter

112
00:09:25,440 --> 00:09:30,360
encryption key, for our session key, in
fact never leaves the TPM. Once we have

113
00:09:30,360 --> 00:09:35,600
created our TPM session, we must include
this session for the execution of any

114
00:09:35,600 --> 00:09:41,160
following TPM command. Usually when we
use tpm2-tools there's an argument if a

115
00:09:41,160 --> 00:09:46,720
tool supports this. And the tool supports
it if the command supports it. This is a

116
00:09:46,720 --> 00:09:52,079
-S. And we need to provide
the session context that we created when

117
00:09:52,079 --> 00:09:57,760
we started the session. Let's take a look.
First we generate a primary key.

118
00:09:57,760 --> 00:10:03,360
By default this is an RSA 2048 bit
key. Then we use the tpm2_startauthsession

119
00:10:03,360 --> 00:10:10,120
tool, and we point that we want to
store the session context with a -S

120
00:10:10,120 --> 00:10:16,120
capital. Later we reuse the session by
feeding it to the tpm2_create tool.

121
00:10:16,120 --> 00:10:22,000
And if you notice, we have specified a policy
session for this example. Please also

122
00:10:22,000 --> 00:10:28,240
note that we have pointed to our primary
key when we start the session. Here is

123
00:10:28,240 --> 00:10:33,720
why. In order to feed entropy to our
session key and enable parameter

124
00:10:33,720 --> 00:10:39,360
encryption, we need to provide some kind
of object either salt, that is just user

125
00:10:39,360 --> 00:10:44,639
entropy, or more than that, binding to an
object. And we can do both at the same

126
00:10:44,639 --> 00:10:50,800
time using the -c that is lowercase
c. Also we can specify one of the three

127
00:10:50,800 --> 00:10:56,320
types of sessions: trial, policy, or HMAC
session. And here you'll notice there is

128
00:10:56,320 --> 00:11:01,079
one extra type, audit session, which is
again based on an HMAC session. 

129
00:11:01,079 --> 00:11:06,279
By default we start a trial session that is
used to create a policy. So it is very

130
00:11:06,279 --> 00:11:09,600
important when we want to enable
parameter encryption to make sure to

131
00:11:09,600 --> 00:11:14,800
specify either HMAC session or policy
session. And here is a good explanation

132
00:11:14,800 --> 00:11:19,399
of what the general difference between
policy and HMAC session. I would

133
00:11:19,399 --> 00:11:23,600
recommend reading the "A Practical Guide to
TPM 2.0" book that is available in the

134
00:11:23,600 --> 00:11:29,079
public domain. Just keep in mind it is
highly technical and very detailed. So

135
00:11:29,079 --> 00:11:33,959
you would need to have some preliminary
understanding of how the TPM works.

136
00:11:33,959 --> 00:11:39,040
Otherwise you might get overwhelmed. Also
reading this book does not exclude

137
00:11:39,040 --> 00:11:44,800
having to read the TPM specifications.
Especially part two about TPM structures

138
00:11:44,800 --> 00:11:50,320
and part three about the commands. The
screenshots we saw are actually from the

139
00:11:50,320 --> 00:11:55,279
part three of the TPM specification
about the TPM commands. Here is an

140
00:11:55,279 --> 00:12:00,639
example of how to protect the creation of
digital signature when using the TPM.

141
00:12:00,639 --> 00:12:07,079
Remember we need to use a TPM object to
feed the entropy for the generation of

142
00:12:07,079 --> 00:12:11,440
the session key. At the same time when we
operate with the TPM we want to use a

143
00:12:11,440 --> 00:12:17,279
TPM key. As usual we generate our primary
key, then we generate our child key, and

144
00:12:17,279 --> 00:12:22,079
afterwards we generate the session. Did
you spot the mistake here? Why didn't we

145
00:12:22,079 --> 00:12:26,839
generate the session right after the
primary key generation? The primary key

146
00:12:26,839 --> 00:12:31,240
private material never leaves the TPM
we can use that the moment we already

147
00:12:31,240 --> 00:12:36,079
have the key generated and start a
session with parameter encryption, so we

148
00:12:36,079 --> 00:12:42,120
protect the generation of our child key.
In this particular example, I did not

149
00:12:42,120 --> 00:12:46,040
place any limitation on the
authorization of the key. There's no

150
00:12:46,040 --> 00:12:50,399
password protection. So it didn't make
much of a difference anyway. But in a

151
00:12:50,399 --> 00:12:55,720
real case scenario, remember to always
start your parameter encryption as early

152
00:12:55,720 --> 00:13:00,279
as possible. Typically I would say use
the endorsement key as entropy for salt

153
00:13:00,279 --> 00:13:04,680
and binding of the TPM session. Going
back to the example now that we have

154
00:13:04,680 --> 00:13:10,399
generated our TPM session, for parameter
encryption, we must feed that to the

155
00:13:10,399 --> 00:13:15,440
tpm2_sign tool. Notice that here the switch to
feed the session is a bit different it's

156
00:13:15,440 --> 00:13:22,079
a -p. Also we're specifying that we are
passing a session context, and then is

157
00:13:22,079 --> 00:13:26,360
the file name of the actual context file.
Let's have another example of how to use

158
00:13:26,360 --> 00:13:31,240
parameter encryption. Here we protect our
non-volatile write and read operations to

159
00:13:31,240 --> 00:13:36,160
the TPMs memory. We start the TPM session
right after the generation of our

160
00:13:36,160 --> 00:13:41,120
primary key. And then we feed that
session using the -S switch to

161
00:13:41,120 --> 00:13:46,600
the (TPM2_)NV_Define command, using the (tpm2)_nvdefine tool.
The (tpm2)_nvwrite and (tpm2)_nvread commands have

162
00:13:46,600 --> 00:13:52,440
different syntaxes. They use a
-P switch, and also require to specify

163
00:13:52,440 --> 00:13:57,720
that we're passing a session. And by
using the plus sign we can add our

164
00:13:57,720 --> 00:14:01,920
password authorization for the NV
index. If you remember the screenshots we

165
00:14:01,920 --> 00:14:05,759
look at the beginning, this guarantees
that the user data we are feeding to the

166
00:14:05,759 --> 00:14:11,440
TPM is sent between the host, between our
program, and the TPM, in encrypted form.

167
00:14:11,440 --> 00:14:16,360
And it will be decrypted inside the TPM and
directly stored to the TPM non-volatile

168
00:14:16,360 --> 00:14:21,920
storage. In a similar fashion, the NV_Read
operation will give us encrypted

169
00:14:21,920 --> 00:14:26,959
output of our user data when it is read
from the NVRAM. And it will be

170
00:14:26,959 --> 00:14:32,440
decrypted by the TSS stack before we can
receive it in our application. And when

171
00:14:32,440 --> 00:14:36,720
our application lives in a memory
isolated environment, or it is a trusted

172
00:14:36,720 --> 00:14:41,519
execution environment, this provides the
highest guarantees that our application

173
00:14:41,519 --> 00:14:46,440
and our data is secure from end to end.
This is why using parameter encryption

174
00:14:46,440 --> 00:14:51,120
is important. Do not forget to start it
at early on. And do not forget to supply

175
00:14:51,120 --> 00:14:56,600
the TPM session for parameter encryption
when issuing the different tpm2 commands, 

176
00:14:56,600 --> 00:15:01,720
and using the different tpm2-tools. For
any questions please write to us in the

177
00:15:01,720 --> 00:15:06,199
OST2 discussion boards after each unit
this helps us understand the context of

178
00:15:06,199 --> 00:15:11,480
your question of course you can always
reach us at our course

179
00:15:11,480 --> 00:15:14,480
email.